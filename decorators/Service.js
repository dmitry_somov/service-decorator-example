const services = {};

function getService(serviceName) {
  return services[serviceName];
}

function ServiceDecorator(Service) {
  services[Service.name] = Service;

  Service.prototype.getService = getService;

  return Service;
}

module.exports = ServiceDecorator;
