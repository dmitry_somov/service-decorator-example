const ServiceDecorator = require('./decorators/Service');

class BService {
  hello() {
    console.log('Hello B');
  }

  helloFromA() {
    const AService = this.getService('AService');

    new AService().hello();
  }
}

module.exports = ServiceDecorator(BService);
