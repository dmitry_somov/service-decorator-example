const ServiceDecorator = require('./decorators/Service');

class AService {
  hello() {
    console.log('Hello A');
  }

  helloFromB() {
    const BService = this.getService('BService');

    new BService().hello();
  }
}

module.exports = ServiceDecorator(AService);
